FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "email-#{n}@example.com" }
    password { 'Passw0rd!' }
    role

    trait :supervisor do
      role { :supervisor }
    end
  end
end
