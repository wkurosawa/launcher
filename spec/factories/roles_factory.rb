FactoryBot.define do
  factory :role do
    name { 'no-role' }
    abilities { [] }

    trait :supervisor do
      name { 'supervisor' }
      abilities { ['create_micro_satellites', 'edit_micro_satellites', 'delete_micro_satellites'] }
    end

    trait :monitor do
      name { 'monitor' }
      abilities { ['edit_micro_satellites'] }
    end

    trait :guest do
      name { 'guest' }
      abilities { [] }
    end
  end
end
