require 'rails_helper'
require 'json_web_token'

RSpec.describe AuthenticateUser, type: :service do
  subject { AuthenticateUser }
  let!(:user) { create(:user) }

  describe '#call' do
    context 'with correct password' do
      it 'returns the encoded token for the user' do
        jwt = subject.call(user.email, 'Passw0rd!')
        expect(JsonWebToken.decode(jwt)['user_id']).to eq(user.id)
      end
    end

    context 'with incorrect password' do
      it 'returns nil' do
        jwt = subject.call(user.email, 'wrong!')

        expect(jwt).to be_nil
      end
    end
  end
end
