
RSpec.describe AuthenticationController, type: :request do
  let!(:user) { create(:user) }

  describe 'POST #authenticate' do
    context 'with jwt of existent user' do
      it 'returns success' do
        post '/authenticate', as: :json, params: { email: user.email, password: 'Passw0rd!' }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with jwt of non existent user' do
      it 'returns error' do
        post '/authenticate', as: :json, params: { email: user.email, password: 'wrong!' }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
