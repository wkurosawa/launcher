require 'json_web_token'

RSpec.describe MicroSatellitesController, type: :request do
  describe 'POST #create' do
    context 'without authorization header' do
      it 'get unauthorized response' do
        post '/micro-satellites', as: :json
        expect(response).to have_http_status(401)
      end
    end

    context 'with supervisor user' do
      let(:role) { create(:role, :supervisor) }
      let(:supervisor) { create(:user, role: role) }
      let(:token) { JsonWebToken.encode(user_id: supervisor.id) }

      it 'can create micro satellites that belongs to the user' do
        params = {
          micro_satellite: {
            name: 'one'
          }
        }

        post '/micro-satellites', as: :json, params: params, headers: { 'Authorization': "Bearer #{token}" }
        expect(response).to have_http_status(201)
        expect(response.parsed_body['micro_satellite']['name']).to eq('one')
        expect(response.parsed_body['micro_satellite']['user_id']).to eq(supervisor.id)
      end
    end

    context 'with monitor user' do
      let(:role) { create(:role, :monitor) }
      let(:monitor) { create(:user, role: role) }
      let(:token) { JsonWebToken.encode(user_id: monitor.id) }

      it 'can not create micro satellites' do
        params = {
          micro_satellite: {
            name: 'two'
          }
        }

        post '/micro-satellites', as: :json, params: params, headers: { 'Authorization': "Bearer #{token}" }
        expect(response).to have_http_status(403)
      end

      it 'can edit micro satellites from another user' do
        another_user = create(:user)
        micro_satellite = create(:micro_satellite, name: 'one', user: another_user)

        params = {
          micro_satellite: {
            name: 'two'
          }
        }

        put "/micro-satellites/#{micro_satellite.id}", as: :json, params: params, headers: { 'Authorization': "Bearer #{token}" }
        expect(response).to have_http_status(202)
        expect(response.parsed_body['micro_satellite']['name']).to eq('two')
      end
    end

    context 'with guest user' do
      let(:role) { create(:role, :guest) }
      let(:guest) { create(:user, role: role) }
      let(:token) { JsonWebToken.encode(user_id: guest.id) }

      it 'can not create micro satellites' do
        params = {
          micro_satellite: {
            name: 'two'
          }
        }

        post '/micro-satellites', as: :json, params: params, headers: { 'Authorization': "Bearer #{token}" }
        expect(response).to have_http_status(403)
      end

      it 'can not edit micro satellites from another user' do
        another_user = create(:user)
        micro_satellite = create(:micro_satellite, name: 'one', user: another_user)

        params = {
          micro_satellite: {
            name: 'two'
          }
        }

        put "/micro-satellites/#{micro_satellite.id}", as: :json, params: params, headers: { 'Authorization': "Bearer #{token}" }
        expect(response).to have_http_status(403)
      end
    end
  end
end
