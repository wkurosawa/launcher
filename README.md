# The Launcher

Manage your micro satellites!

## Features

- User authentication
- User ACL (access control list) through Roles
- Supervisor role can create and edit MicroSatellites
- Monitor role can only edit MicroSatellites

## Configuration

### Ruby version
2.6.4

### Database creation
```
rake db:create
```

### Database initialization
```
rake db:migrate && rake db:seed
```

## Run test suite

```
rspec
```

## Deployment instructions

Push to Heroku app

# Heroku app usage

Deployed to
```
https://satellites-launcher.herokuapp.com/
```

## Users

__Supervisor__
- Name: Neil Armstrong
- Email: neil@nasa.com
- Password: neil123

__Monitor__
- Name: Buzz Aldrin
- Email: buzz@nasa.com
- Password: buzz456

## Authenticate
```
POST https://satellites-launcher.herokuapp.com/authenticate
```
__Params__
```json
{
  "email": "buzz@nasa.com",
  "password": "buzz456"
}
```

__Response__

A token to be used on requests as Authorization header

```json
{
  "auth_token": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJleHAiOjE1NzIzNjUwNTN9.7dIFsMjKsm4f5AIZd3oT73MxwtE6idP-9q3TJR7jQwQ"
}
```

## MicroSatellites

### Index
```
GET https://satellites-launcher.herokuapp.com/micro-satellites
```

__Response__
```json
{
  "micro_satellites": [
    {
      "id": 1,
      "name": "Test1",
      "user_id": 1,
      "created_at": "2019-10-28T16:08:59.641Z",
      "updated_at": "2019-10-28T16:08:59.641Z"
    }
  ]
}
```

### Create
Only the user with a supervisor role is able to create MicroSatellites

```
POST https://satellites-launcher.herokuapp.com/micro-satellites
```
__Headers__
```
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NzIzNjUzMjR9.3PKSpO8ioFDTV12CDp19jF31mPDzQFQgbht51VBFnqY
Content-Type: text/plain; charset=utf-8
```

__Params__
```json
{
  "micro_satellite": {
    "name": "Test1"
  }
}
```

__Response__
```json
{
  "micro_satellite": {
    "name": "Test1",
    "user_id": 1
  }
}
```


If a user without permission tries to create it will have a 403 Forbidden error:
```json
{
  "message": "Forbidden"
}
```

### Update
Supervisor and Monitor roles can update MicroSatellites

```
PUT https://satellites-launcher.herokuapp.com/micro-satellites/1
```
__Headers__
```
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NzIzNjUzMjR9.3PKSpO8ioFDTV12CDp19jF31mPDzQFQgbht51VBFnqY
Content-Type: text/plain; charset=utf-8
```

__Params__
```json
{
  "micro_satellite": {
    "name": "New"
  }
}
```

__Response__
```json
{
  "micro_satellite": {
    "name": "New",
    "user_id": 1
  }
}
```
