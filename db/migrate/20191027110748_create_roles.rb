class CreateRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :roles do |t|
      t.string :name
      t.string :abilities, array: true, default: []

      t.timestamps
    end
  end
end
