MicroSatellite.destroy_all
User.destroy_all
Role.destroy_all

roles_data = [
  { name: 'supervisor', abilities: ['create_micro_satellites', 'edit_micro_satellites', 'delete_micro_satellites']},
  { name: 'monitor', abilities: ['edit_micro_satellites']},
]

puts "Creating Roles"
roles_data.each do |role_data|
  Role.create(role_data)
end

supervisor = Role.find_by(name: 'supervisor')
monitor = Role.find_by(name: 'monitor')

users_data = [
  { name: 'Neil Armstrong', email: 'neil@nasa.com', password: 'neil123', role_id: supervisor.id },
  { name: 'Buzz Aldrin', email: 'buzz@nasa.com', password: 'buzz456', role_id: monitor.id },
]

puts "Creating Users"
users_data.each do |user_data|
  User.create(user_data)
end
