# A thin wrapper that gives classes a callable interface via {ClassMethods#call}.
module Service
  extend ActiveSupport::Concern

  module ClassMethods
    # Allows you to call a service without having to instantiate it
    def call(*args)
      new(*args).call
    end
  end
end
