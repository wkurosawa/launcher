class AuthenticationController < ApplicationController
  skip_before_action :authenticate_request

  def authenticate
    jwt = AuthenticateUser.call(params[:email], params[:password])

    if jwt
      render json: { auth_token: jwt }
    else
      render json: { error: 'Not authorized' }, status: :unauthorized
    end
  end
end
