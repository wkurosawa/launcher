class ApplicationController < ActionController::API
  class Forbidden < StandardError
    def initialize
      super('Forbidden content')
    end
  end

  before_action :authenticate_request
  attr_reader :current_user

  rescue_from Forbidden, with: :forbidden_response

  private

  def authenticate_request
    @current_user = AuthorizeApiRequest.call(request.headers)
    render json: { error: 'Not Authorized' }, status: 401 unless @current_user
  end

  def forbidden_response(e)
    render json: { message: 'Forbidden' }, status: 403
  end
end
