class MicroSatellitesController < ApplicationController
  skip_before_action :authenticate_request, only: :index
  before_action :check_resource_create, only: :create
  before_action :set_micro_satellite, :check_resource_update, only: :update

  def index
    @micro_satellites = MicroSatellite.all

    render json: { micro_satellites: @micro_satellites }, status: 200
  end

  def create
    @micro_satellite = current_user.micro_satellites.create!(micro_satellite_params)

    result = {
      micro_satellite: {
        name: @micro_satellite.name,
        user_id: @micro_satellite.user_id
      }
    }

    render json: result, status: 201
  end

  def update
    @micro_satellite.update(micro_satellite_params)

    result = {
      micro_satellite: {
        name: @micro_satellite.name,
        user_id: @micro_satellite.user_id
      }
    }

    render json: result, status: 202
  end

  private

  def micro_satellite_params
    params.require(:micro_satellite).permit(:name)
  end

  def set_micro_satellite
    @micro_satellite = MicroSatellite.find(params[:id])
  end

  def check_resource_create
    raise Forbidden if  !current_user.role.abilities.include?('create_micro_satellites')
  end

  def check_resource_update
    raise Forbidden if  !current_user.role.abilities.include?('edit_micro_satellites')
  end
end
