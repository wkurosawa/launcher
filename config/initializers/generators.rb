Rails.application.config.generators do |g|
  g.test_framework  :rspec,
                    fixture: false,
                    fixture_replacement: :factory_bot
  g.factory_bot suffix: "factory"
end
